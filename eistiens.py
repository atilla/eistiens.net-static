from flask import Flask, render_template, url_for
from flask_frozen import Freezer

from associations import read_associations

app = Flask(__name__)
app.config["FREEZER_RELATIVE_URLS"] = True

freezer = Freezer(app)


@app.route("/")
def index():
    associations = read_associations()
    associations.sort(key=lambda association: association["name"])

    return render_template("index.html", associations=associations)

if __name__ == "__main__":
    freezer.freeze()
