import os
import yaml

YAML_FOLDER = "associations"


class Association:
    def __init__(self, yamlpath):
        with open(yamlpath) as stream:
            self.data = yaml.load(stream)

    # Allow use of association["name"] instead of association.data["name"]
    def __getitem__(self, key):
        return self.data[key]


def read_associations():
    """Read associations metadata files.

    A file is considered an association file if it ends with .yaml
    and is located in YAML_FOLDER
    """
    return [Association(os.path.join(YAML_FOLDER, file))
            for file in os.listdir(YAML_FOLDER)
            if file.endswith(".yaml")]
