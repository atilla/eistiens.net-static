# Eistiens.net-static

This is the repo dedicated to the static version of eistiens.net.
As of now, we're only designing the mockup, suggestions are appreciated.

## Installation

Eistiens.net-static is powered by Frozen-Flask which builds a static version
of a Flask application.

To build the application, run the following commands

    $ virtualenv env
    $ . env/bin/activate
    $ pip install -r requirements.txt
    $ python eistiens.py

A whole new `build/` folder will be created, containing the frozen application.
Open `build/index.html` in your favorite browser to admire the result.

## Add an association

In order to add an association, you just have to create its `.yaml` metadata
file.

* Create a new file `associations/ASSOCIATION_NAME.yaml`
* Use the following template and put the informations of the new association

```{.yaml}
name: Atilla
image: img/atilla.png
description: |
    Atilla est l'association d'informatique de l'EISTI, dédiée à la promotion du
    logiciel libre au sein de l'EISTI. Atilla organise des formations, s'occupe
    d'héberger les sites des listes des différentes associations de l'EISTI, et
    propose des canapés immensément confortables pour les joueurs de contrée.
links:
    - name: Site internet
      url: https://atilla.org
    - name: Twitter
      url: https://twitter.com/CY106

```

Run `python eistiens.py` again, and the new association should appear on the page!
Associations are sorted according to their name.
